import React from "react";
import Header from "./header/header";
import Container from "./container";
function Layout(){

    return(
        <div>
            <Header />
            <Container/>
        </div>
    )
}
export default Layout;