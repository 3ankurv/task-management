import React from "react";
import {BrowserRouter,Switch,Route,Redirect} from "react-router-dom";
import TaskContainer from "./task/TaskContainer";
function Container(){

    return(
        <div>
             <BrowserRouter>
                <Switch>
                   <Route path="/">
                        <TaskContainer />
                    </Route>
                </Switch>
                </BrowserRouter>
        </div>
    )
}
export default Container;