import { TaskStatus } from "../task-status.enum";
export declare class TaskSearchDto {
    status: TaskStatus;
    search: string;
}
