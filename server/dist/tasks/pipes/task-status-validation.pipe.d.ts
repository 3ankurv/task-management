import { PipeTransform } from "@nestjs/common";
import { TaskStatus } from "../task.model";
export declare class TaskStatusValidationPipe implements PipeTransform {
    readonly allowedStatus: TaskStatus[];
    transform(value: any): any;
    isValidStatus(status: any): boolean;
}
