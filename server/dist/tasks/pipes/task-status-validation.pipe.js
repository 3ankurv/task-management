"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = require("@nestjs/common");
const task_model_1 = require("../task.model");
class TaskStatusValidationPipe {
    constructor() {
        this.allowedStatus = [
            task_model_1.TaskStatus.OPEN,
            task_model_1.TaskStatus.DONE,
            task_model_1.TaskStatus.IN_PROGRESS
        ];
    }
    transform(value) {
        if (!this.isValidStatus(value)) {
            throw new common_1.BadRequestException();
        }
        return value;
    }
    isValidStatus(status) {
        let idx = this.allowedStatus.indexOf(status);
        return idx !== -1;
    }
}
exports.TaskStatusValidationPipe = TaskStatusValidationPipe;
//# sourceMappingURL=task-status-validation.pipe.js.map