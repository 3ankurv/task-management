import { TasksService } from './tasks.service';
import { TaskStatus } from './task-status.enum';
import { CreateTaskDto } from "./dto/create-task.dto";
import { TaskSearchDto } from './dto/task-search.dto';
import { Task } from './task.entity';
import { User } from 'src/auth/user.entity';
export declare class TasksController {
    private taskService;
    constructor(taskService: TasksService);
    getAllTasks(filterDto: TaskSearchDto, user: User): any;
    getTaskById(id: number, user: User): Promise<Task>;
    deleteTaskById(id: number, user: User): Promise<void>;
    updateTaskStatus(id: number, status: TaskStatus, user: User): Promise<Task>;
    createNewTask(createTaskDto: CreateTaskDto, user: User): Promise<Task>;
}
