import { Repository } from "typeorm";
import { Task } from "./task.entity";
import { CreateTaskDto } from "./dto/create-task.dto";
import { TaskSearchDto } from "./dto/task-search.dto";
import { User } from "src/auth/user.entity";
export declare class TaskRepository extends Repository<Task> {
    createTask(createTaskDto: CreateTaskDto, user: User): Promise<Task>;
    getAllTask(taskSearchDto: TaskSearchDto, user: User): Promise<Task[]>;
}
