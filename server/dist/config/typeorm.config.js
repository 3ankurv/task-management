"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const task_entity_1 = require("../tasks/task.entity");
const user_entity_1 = require("../auth/user.entity");
console.log(__dirname);
exports.typeOrmConfig = {
    type: "postgres",
    host: "localhost",
    port: 5432,
    username: "postgres",
    password: "admin",
    database: "taskmanagment",
    entities: [task_entity_1.Task, user_entity_1.User],
    dropSchema: false,
    logging: true,
    synchronize: true
};
//# sourceMappingURL=typeorm.config.js.map