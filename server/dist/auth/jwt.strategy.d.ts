import { IJwtPayload } from "./jwt-payload.interface";
import { UserRepository } from "./user.repositoty";
import { User } from "./user.entity";
declare const JwtStrategy_base: new (...args: any[]) => any;
export declare class JwtStrategy extends JwtStrategy_base {
    private userRepository;
    constructor(userRepository: UserRepository);
    validate(payload: IJwtPayload): Promise<User>;
}
export {};
