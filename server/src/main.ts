import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import * as config from "config"
console.log();
const serverConfig = config.get("server");
console.log(process.env);
async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  app.enableCors();
  const port = serverConfig.port;
  await app.listen(port);
}
bootstrap();
