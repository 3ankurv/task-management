import {PassportStrategy} from "@nestjs/passport";
import {Strategy,ExtractJwt} from "passport-jwt";
import { Injectable, UnauthorizedException } from "@nestjs/common";
import {IJwtPayload} from "./jwt-payload.interface";
import { Repository } from "typeorm";
import { UserRepository } from "./user.repositoty";
import { InjectRepository } from "@nestjs/typeorm";
import { User } from "./user.entity";

@Injectable()
export class JwtStrategy extends PassportStrategy(Strategy){

    constructor(
        @InjectRepository(UserRepository)
       private userRepository:UserRepository
    ){
        super({
            jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
            secretOrKey:"topSecret91"
        })
    }

    async validate(payload:IJwtPayload) : Promise<User> {

        const user = this.userRepository.findOne(payload)
        if(!user){
            throw new UnauthorizedException();
        }

        return user;
    }

}
