import { Injectable, UnauthorizedException } from '@nestjs/common';
import { UserRepository } from './user.repositoty';
import { InjectRepository } from '@nestjs/typeorm';
import { AuthCredentialDto } from './dto/auth-credential.dto';
import { JwtService } from '@nestjs/jwt';
import { IJwtPayload } from "./jwt-payload.interface";

@Injectable()
export class AuthService {

    constructor(
        @InjectRepository(UserRepository)
        private userRepository:UserRepository,
        private jwtService:JwtService){
    }

    signUp(authCredentialDto:AuthCredentialDto){
        return this.userRepository.createUser(authCredentialDto);
    }

   async signIn(authCredentialDto:AuthCredentialDto) : Promise<{accessToken:string}>{
        const username  = await this.userRepository.signIn(authCredentialDto);
        if(!username){
            throw new UnauthorizedException("Invalid Credntials")
        }
        // : IJwtPayload
        const payload = { username };
        const accessToken = await this.jwtService.sign(payload);
        
        return {accessToken};
    }
}
