import { Repository, EntityRepository } from "typeorm";
import { User } from "./user.entity";
import {AuthCredentialDto} from "./dto/auth-credential.dto";
import { ConflictException, InternalServerErrorException } from "@nestjs/common";
import * as bcrypt from "bcrypt";

@EntityRepository(User)
export class UserRepository extends Repository<User> {

    async createUser(authCredentialDto:AuthCredentialDto):Promise<void>{
        const { username,password} = authCredentialDto;
        const user = new User();
        const salt = await bcrypt.genSalt();
        user.username = username;
        user.salt = salt;
        user.password = await this.hasPassword(password,salt);

        try{
            await user.save();
        }catch(err){
                if(err.code ==="23505"){
                    throw new ConflictException("Username alrady exists");
                }
                else{
                    throw new InternalServerErrorException();
                }
        }
        
    }

    async signIn(authCredentialDto:AuthCredentialDto)  : Promise<String>{
        
        const {username,password} = authCredentialDto;
        const user  = await this.findOne({username});
        if(user && await user.validatePassword(password)){
            return username;
        }else{
            return null;
        }
        
    }

    private async hasPassword(password:string,salt:string){
        return await bcrypt.hash(password,salt);
    }
}