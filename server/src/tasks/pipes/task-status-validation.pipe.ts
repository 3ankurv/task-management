import { PipeTransform, BadRequestException } from "@nestjs/common";
import { TaskStatus } from "../task.model";

export class TaskStatusValidationPipe implements PipeTransform{

    readonly allowedStatus = [
        TaskStatus.OPEN,
        TaskStatus.DONE,
        TaskStatus.IN_PROGRESS
    ]
    transform(value: any) {

        if(!this.isValidStatus(value)){
            throw new BadRequestException()
        }
       // throw new Error("Method not implemented.");
       return value;
    }

    isValidStatus(status:any){
      let idx =   this.allowedStatus.indexOf(status);
      return idx!==-1;
    }

}