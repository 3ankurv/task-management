import { Controller, Get, Post, Body, Param, Delete, Patch, Query, UsePipes, ValidationPipe, ParseIntPipe, UseGuards } from '@nestjs/common';
import { TasksService } from './tasks.service';
import {TaskStatus } from './task-status.enum';
import {CreateTaskDto} from "./dto/create-task.dto";
import { TaskSearchDto } from './dto/task-search.dto';
import { TaskStatusValidationPipe } from './pipes/task-status-validation.pipe';
import { Task } from './task.entity';
import { AuthGuard } from '@nestjs/passport';
import { User } from 'src/auth/user.entity';
import { GetUser } from 'src/auth/auth-user.decorater';

@Controller('tasks')
@UseGuards(AuthGuard())
export class TasksController {
    constructor(private taskService:TasksService){}


    @Get()
    getAllTasks(@Query(ValidationPipe) filterDto:TaskSearchDto , @GetUser() user:User):any{
        console.log(filterDto);
        if(Object.keys(filterDto).length){
        
        }
        return this.taskService.getAllTasks(filterDto,user);
    }

    @Get("/:id")
    getTaskById(@Param("id",ParseIntPipe) id:number,@GetUser() user:User): Promise<Task> {
        return this.taskService.getTaskById(id,user);
    }

    @Delete("/:id")
    deleteTaskById(@Param("id",ParseIntPipe) id:number,@GetUser() user:User){
        return this.taskService.deleteTaskById(id,user);
    }

    @Patch("/:id/status")
    updateTaskStatus(@Param("id",ParseIntPipe) id:number,@Body("status",TaskStatusValidationPipe) status:TaskStatus,@GetUser()user:User):Promise<Task>{
        return this.taskService.updateTask(id,status,user);
    }

    @Post()
    @UsePipes(ValidationPipe)
    createNewTask(@Body() createTaskDto: CreateTaskDto,
   @GetUser() user:User){
        return this.taskService.createTask(createTaskDto,user);
    }
}
