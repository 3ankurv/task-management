import { Injectable, NotFoundException } from '@nestjs/common';
//import * as uuid from "uuid/v1";
import { CreateTaskDto } from './dto/create-task.dto';
import { TaskRepository } from './task.repository';
import { InjectRepository } from '@nestjs/typeorm';
import { Task } from './task.entity';
import { TaskStatus } from './task-status.enum';
import { TaskSearchDto } from './dto/task-search.dto';
import { User } from 'src/auth/user.entity';
import { GetUser } from 'src/auth/auth-user.decorater';
import { userInfo } from 'os';

@Injectable()
export class TasksService {
    
    constructor(@InjectRepository(TaskRepository) private taskRepository:TaskRepository){

    }
    //private tasks : Task[]= [];
    getAllTasks(filterDto:TaskSearchDto,user:User):Promise<Task[]>{

        return this.taskRepository.getAllTask(filterDto,user);
    }

   async  getTaskById(id:number,user:User): Promise<Task> {

       const found  = await this.taskRepository.findOne({where:{id,userId:user.id}});

       if(!found){
           throw new NotFoundException(`Task not found with id ${id}`);
       }
       return found;

    }

    async deleteTaskById(id:number,@GetUser() user:User):Promise<void>{
         const result = await  this.taskRepository.delete({id,userId:user.id});
            if(result.affected===0){
                throw new NotFoundException(`Task Id ${id} not found`)
            }
    }

   async createTask(createTaskDto:CreateTaskDto,user:User) : Promise<Task> {
         return this.taskRepository.createTask(createTaskDto,user);
          
        // const task : Task= {
        //     id:uuid(),
        //     title,
        //     description,
        //     status:TaskStatus.OPEN
        // }

        // this.tasks.push(task);
        // return  task;
        return null;
    }

   async updateTask(id:number,status:TaskStatus,user:User):Promise<Task>{
         const task = await this.getTaskById(id,user);
         task.status = status;
         await task.save();
         return task
    }
}
