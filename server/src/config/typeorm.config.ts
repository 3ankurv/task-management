import {TypeOrmModuleOptions} from "@nestjs/typeorm";
import {Task} from "../tasks/task.entity";
import {User} from "../auth/user.entity";
console.log(__dirname);
export const typeOrmConfig: TypeOrmModuleOptions = {
  type:"postgres",
  host:"localhost",
  port: 5432,
  username:"postgres",
  password:"admin",
  database:"taskmanagment",
  entities: [ Task, User],
  dropSchema: false,
  logging: true,
  synchronize:true

}